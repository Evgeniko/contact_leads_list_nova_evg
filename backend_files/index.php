<?php
    header('Access-Control-Allow-Origin: *');
?>

<div id="contact_leads_list">
    <script>
    $(function(){

      var current_entity = AMOCRM.data.current_entity;
      var is_card = AMOCRM.data.is_card;
      if ((current_entity == 'leads') && (is_card)) {
          lead_id = AMOCRM.data.current_card.id;
          startPreloader();
          renderList();
      }

      function renderList(){
        var contacts_leads = {}; // Объект с контактами и их сделками
        var contacts_ids = []; //массив нужных контактов
        var leads_ids = []; //массив нужных сделок

        //Находим id контактов со страницы сделки
        $('#contacts_list [name="ID"]').each(function(i){
            var id = $(this).val();
          contacts_ids.push(id);
          contacts_leads[id] = {};
        });

        if(contacts_ids.length){ //Если есть контакты
          var statusesSort = {}; //Отсортированные статусы сделок по id
          //Получение статусов сделок/воронок
          $.get('/api/v2/account', {with: 'pipelines'}, function(data){
            var pips = data._embedded.pipelines;
            _.each(pips, function(pip, i){
              var pip_name = pip.name;
              var statuses = pip.statuses;
              _.each(statuses, function(status, i){
                var id = status.id;
                statusesSort[id] = status;
              });
            });
            //Получение контактов по id шникам
            $.get('/api/v2/contacts', {id: contacts_ids}, function(contacts){
              contacts = contacts._embedded.items;
              console.log(contacts);
              _.each(contacts, function(contact, i){
                var contact_id = contact.id;
                var leads = contact.leads.id;
                _.each(leads, function(id, k){
                  console.log(id);
                  if (id != lead_id) {
                      leads_ids.push(id);
                      contacts_leads[contact_id][id] = [];
                  }
                });
              });
              console.log(contacts_leads);
              if (leads_ids.length) {
                //Получение сделок этих контактов
                $.get('/api/v2/leads', {id: leads_ids}, function(leads){
                  leads = leads._embedded.items;
                  console.log(leads);
                  _.each(leads, function(lead, i){
                    var leadId = lead.id;
                    var contactsIds = lead.contacts.id;
                    _.each(contactsIds, function(id, i){
                      if (contacts_leads[id]) {
                        contacts_leads[id][leadId] = lead;
                      }
                    });
                  });
                  var contacts_leads_sort = {}; //Сортированные по статусам сделки контактов
                  //Сортировка сделок по статусам
                  _.each(contacts_leads, function (cl, i) {
                    contacts_leads_sort[i] = _.sortBy(cl, function(val){
                      var result = 0;
                      if (val.status_id == 142) {
                        result = 998;
                      }else if (val.status_id == 143) {
                        result = 999;
                      }else {
                        var id = val.status_id;
                        result = statusesSort[id].sort;
                      }
                      return result;
                    });
                  });
                  //Отрисовка списка сделок над контактом
                  _.each(contacts_leads_sort, function(leads, contactId){
                    var leadsListHTML = '<h6>Другие сделки контакта</h6>';
                    _.each(leads, function(lead, i){
                      var status_id = lead.status_id;
                      var statusLead = statusesSort[status_id];
                      leadsListHTML += '<a href="/leads/detail/'+lead.id+'" target="_blank" class="el js-navigate-link">' +
                              '<b>'+lead.name+'</b><br>' +
                              '<span class="pipe">'+statusLead.name+'</span>' +
                          '</a>';
                    });
                    var target = $('[name="ID"][value="'+contactId+'"]').closest('.linked-forms__item');
                    if ($(target).find('.contact_leads_list_ul').length == 0) {
                      $(target).prepend('<ul class="contact_leads_list_ul">'+leadsListHTML+'</ul>');
                    }
                  });
                  //hidePreloader();
                  console.log(contacts_leads_sort);
                  console.log(contacts_leads);
                });
              }
            });
          });
        }
        hidePreloader();
      }
      //Показ предзагрузочной анимации
      function startPreloader() {
          var preloaderHTML = '<div class="container-preloader-evg">' +
              '  <div class="circular-container">' +
              '    <div class="circle circular-loader1">' +
              '      <div class="circle circular-loader2"></div>' +
              '    </div>' +
              '  </div>' +
              '</div>';
          $('#contacts_list [name="ID"]').each(function () {
              $(this).closest('.linked-forms__item').prepend(preloaderHTML);
          });
      }
      function hidePreloader() {
        $('.container-preloader-evg').remove();
      }
    });
    </script>
    <style>
        .contact_leads_list_ul{
            margin-bottom:10px;
        }
        .contact_leads_list_ul .el{
            padding: 15px;
            background: rgba(255,255,255,.8);
            cursor: pointer;
            display: block;
            text-decoration: none;
        }
        .contact_leads_list_ul .el b{
            font-size: 18px;
            font-weight: bold;
        }
        .contact_leads_list_ul .el .pipe{
            display: inline-block;
            position: absolute;
            padding: 4px 10px;
            background: #3b5ab1;
            color: #fff;
            border-radius: 20px;
            font-size: 12px;
        }
        body{
            background-color: #2a2a2a;
            margin: 0;
        }
        .container-preloader-evg{
            text-align: center;
            color: #fff;
        }
        .circular-container{
            width:10%;
            margin: 0 auto;
        }

        .circle{
            border: 5px solid transparent;
            border-radius: 50%;
        }

        .circular-loader1{
            width: 50px;
            height: 50px;
            display: table;
            padding: 1px;
            border-top: 5px solid #006495;
            border-bottom: 5px solid #006495;
            animation: circular_loader1 linear 2s infinite;
        }

        .circular-loader2{
            width: 10px;
            height: 10px;
            display: table-cell;
            border-right: 5px solid #3498db;
            border-left: 5px solid #3498db;
            animation: circular_loader2 linear 2s infinite;
        }

        @keyframes circular_loader1 {
            0% {
                transform: rotate(0deg);
            }
            50% {
                transform: rotate(-90deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        @keyframes circular_loader2 {
            0% {
                transform: rotate(0deg);
            }
            50% {
                transform: rotate(-180deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
</div>
