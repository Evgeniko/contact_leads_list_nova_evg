define(['jquery'], function($){
    var CustomWidget = function () {
        var self = this;
        this.callbacks = {
            render: function(){
                if ($('#contact_leads_list').length) {
                    $('#contact_leads_list').remove();
                }
                $.get('https://dmitrybondar.ru/widgets/api/schooltrip/contact_leads_list/index.php',{},function(data){
                        $('body').append(data);
                });
                return true;
            },
            init: function(){
                return true;
            },
            bind_actions: function(){
                return true;
            },
            settings: function(){
                return true;
            },
            onSave: function(){
                return true;
            },
            destroy: function(){

            },
            contacts: {
                    //select contacts in list and clicked on widget name
                    selected: function(){
                    }
                },
            leads: {
                    //select leads in list and clicked on widget name
                    selected: function(){
                    }
                },
            tasks: {
                    //select taks in list and clicked on widget name
                    selected: function(){
                    }
                }
        };
        return this;
    };

return CustomWidget;
});
